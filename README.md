# README #

I tried to create animated gif with transparent color from png file. I had two versions of images2gif.py.

This version

    https://github.com/rec/echomesh/blob/master/code/python/external/images2gif.py

...fixed:

    "TypeError: must be string or buffer, not None"

...and this version

    https://bitbucket.org/bench/images2gif.py/src

...had support for transparency.


I merged these two... and... miraculously it worked in my use case :-)